import numpy as np
import pandas as pd
import sklearn as skl
from sklearn.linear_model import RidgeCV

def ingest(csv_file_name):
  return(pd.read_csv(csv_file_name).to_numpy())

def export(csv_file_name, data, start_index, stop_index, header):
  print(data)
  indices = np.arange(start_index, stop_index+1, 1)
  final = np.transpose(np.concatenate(([indices], [data]), axis = 0))
  np.savetxt(csv_file_name, final, delimiter=',', header=header)
  return

def train(training_data):
    X = training_data[:,2:]
    y = training_data[:,1]
    X_cols = np.size(X,0)

    quad = np.square(X)
    exp = np.exp(X)
    cos = np.cos(X)
    const = np.ones((X_cols,1))

    X = np.c_[X, quad, exp, cos, const]
    reg = RidgeCV(alphas = [0.01,0.05,0.1,0.5,1,5,10,15]).fit(X, y)
    print(reg)
    return reg.coef_

def __main__():
  training_file = "train.csv"
  training_data = ingest("train.csv")

  model = train(training_data)
  df = pd.DataFrame(model)
  df.to_csv("submission2.csv", header=False, index=False)

if (__name__ =="__main__"):
  __main__()
