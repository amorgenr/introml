import numpy as np
import pandas as pd
import sklearn as skl
from sklearn.linear_model import RidgeCV
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import KFold

def ingest(csv_file_name):
  return(pd.read_csv(csv_file_name).to_numpy())

def export(csv_file_name, data, start_index, stop_index, header):
  print(data)
  indices = np.arange(start_index, stop_index+1, 1)
  final = np.transpose(np.concatenate(([indices], [data]), axis = 0))
  np.savetxt(csv_file_name, final, delimiter=',', header=header)
  return

def train(training_data):
    X = training_data[:,2:]
    y = training_data[:,1]
    lambda_ = [0.01, 0.1, 1, 10, 100]
    sol = []
    for l in lambda_:
        clf = RidgeCV(alphas = [l],cv=10, fit_intercept=False).fit(X,y)
        y_pred = clf.predict(X)
        sol.append(np.sqrt(mean_squared_error(y, y_pred)))
    return sol

def __main__():
  training_file = "train.csv"
  training_data = ingest("train.csv")

  model = train(training_data)
  df = pd.DataFrame(model)
  df.to_csv("submission1.csv", header=False, index=False)

if (__name__ =="__main__"):
  __main__()
