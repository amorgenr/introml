import numpy as np
import pandas as pd
import sklearn as skl
from sklearn.linear_model import RidgeCV
import seaborn as sns

def ingest(csv_file_name):
  return(pd.read_csv(csv_file_name).to_numpy())

def export(csv_file_name, data, start_index, stop_index, header):
  print(data)
  indices = np.arange(start_index, stop_index+1, 1)
  final = np.transpose(np.concatenate(([indices], [data]), axis = 0))
  np.savetxt(csv_file_name, final, delimiter=',', header=header)
  return

def train(training_data, alpha):
  X = training_data[:,2:]
  y = training_data[:,1]
  alphas = [0.01, 0.1, 1, 10, 100, 1e3, 1e4, 2e4, 5e4, 8e4, 1e5, 1e6, 1e7, 1e8]
  clf = RidgeCV(alphas=alphas, store_cv_values=True)
  print(X.shape,y.shape)
  clf.fit(X,y)
  return(clf)

def test(test_data, model):
  X = test_data[:,1:]
  predictions = RidgeCV.predict(model, X)
  return(predictions)

def feature_analysis(training_data):
  X = training_data[:,2:]
  corr = pd.DataFrame(X).corr()
  sns.heatmap(corr, mask=np.zeros_like(corr, dtype=np.bool),
              cmap=sns.diverging_palette(220, 10, as_cmap=True),
              square=True)

def __main__():
  #import data from csv, and transform to numpy array for sklearn input
  training_file = "train.csv"
  testing_file = "test.csv"

  training_data = ingest("train.csv")
  testing_data = ingest("test.csv")

  feature_analysis(training_data)

  model = train(training_data, 0)
  predictions = test(testing_data, model)
  export("output.csv", predictions, 10000, 11999, 'Id,y')

if (__name__ =="__main__"):
  __main__()
