import torch

dtype = torch.float
device = torch.device("cpu")
#device = torch.device("cuda:0")

#Batch size, input dims, hidden dims, output dims

N, D_in, H, D_out = 64, 1000, 100, 10

x = torch.randn(N, D_in)
y = torch.randn(N, D_out)

model = torch.nn.Sequential(
    torch.nn.Linear(D_in, H),
    torch.nn.ReLU(),
    torch.nn.Linear(H, D_out),
)

loss_fn = torch.nn.MSELoss(reduction = 'sum')

learning_rate = 1e-6

optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

for t in range(5000):
    y_pred = model(x)

    loss = loss_fn(y_pred, y)

    if t % 100 == 99:
        print(t, loss.item())

    optimizer.zero_grad()

    loss.backward()

    optimizer.step()
